# Methylome v1.3

**Methylome** workflow , which agree to FAIR principles , was built in Nexflow dsl2 language, with singularity container for used softwares, optimized in terms of computing resources (cpu, memory) for fish genomes studies, and its use on a informatic farm with a slurm scheduler.

- Quality and adapter trimming from paired-end reads was carried out using [Trim Galore](https://github.com/FelixKrueger/TrimGalore) [1].
- Trimmed reads mapping, remove duplicates (if required), and methylation calling was achieved with [Bismark](https://github.com/FelixKrueger/Bismark) [2].
- BigWig files were obtained with [bedGraphToBigWig](http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64.v385/) [3].

## Install methylome flow and build singularity image

Clone methylome git and build local singularity image (with system admin rights) based on the provided singularity definition file.

```bash
git clone https://forgemia.inra.fr/lpgp/methylome.git
sudo singularity build ./methylome/singularity/methylome.sif ./methylome/singularity/methylome.def
```

## Usage examples

design.csv file must have *ID*, *R1* and *R2* header and write with comma separator.

|ID|R1|R2|
|:-|:-|:-|
|A|/path/to/targetA_R1.fa.gz|/path/to/targetA_R2.fa.gz|
|B|/path/to/targetB_R1.fa.gz|/path/to/targetB_R2.fa.gz|
|C|/path/to/targetC_R1.fa.gz|/path/to/targetC_R2.fa.gz|

### RRBS

```bash
#!/bin/bash
#SBATCH -J methylation
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/21.10.6
nextflow run /work/project/lpgp/Nextflow/methylome/ \
-profile slurm \
--input "${PWD}/design.csv" \
--genome "genome.dna.toplevel.fa" \
--rrbs \
--ignore 2 \
--ignore_3prime 2 \
--ignore_r2 2 \
--ignore_3prime_r2 2 \
--scaffolds \
--buffer_size '50G' \
--meth_ratio_matrix \
--bigwig \
--out_dir "${PWD}/results"
```

### RRBS for bisulfite conversion computation

```bash
#!/bin/bash
#SBATCH -J methylation
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/21.10.6
nextflow run /work/project/lpgp/Nextflow/methylome/ \
-profile slurm \
--input "${PWD}/design.csv" \
--genome "${baseDir}/spikes_templates/diagenode/diagenode_spikes.fa" \
--rrbs \
--ignore 2 \
--ignore_3prime 2 \
--ignore_r2 2 \
--ignore_3prime_r2 2 \
--CX \
--out_dir "${PWD}/spikes"
```

### WGBS

```bash
#!/bin/bash
#SBATCH -J methylation
#SBATCH -p unlimitq
module load containers/singularity/3.9.9
module load bioinfo/Nextflow/21.10.6
nextflow run /work/project/lpgp/Nextflow/methylome/ \
-profile slurm \
--input "${PWD}/design.csv" \
--genome "genome.dna.toplevel.fa" \
--three_prime_clip_r1 10 \
--clip_r2 10 \
--scaffolds \
--buffer_size '50G' \
--meth_ratio_matrix \
--bigwig \
--out_dir "${PWD}/results"
```

## Defaults parameters

Please refer to [Trim Galore](https://github.com/FelixKrueger/TrimGalore) and [Bismark](https://github.com/FelixKrueger/Bismark) for complete arguments explanation.

```bash
# fastq input
input = false

# genome index
genome = false
bismarkIndex = false
keep_index = false

# fastqc
skip_fastqc = false

# trimming
skip_trimming = false
clip_r1 = 0
clip_r2 = 0
three_prime_clip_r1 = 0
three_prime_clip_r2 = 0
rrbs = false
keep_trimmed = false

# Bimark alignment
local = false
score_min = 0.2

# bismark_methylation_extractor
ignore = 0
ignore_3prime = 0
ignore_r2 = 0
ignore_3prime_r2 = 0
CX = false
scaffolds = false
buffer_size = false

# merge methyation ratio
meth_ratio_matrix=false

# bigwig
bigwig=false

# save directory
out_dir = "${PWD}/results"
```

## References

1. Krueger F, Galore T. A wrapper tool around cutadapt and FastQC to consistently apply quality and adapter trimming to FastQ files [Internet]. Available from: http://www.bioinformatics.babraham.ac.uk/projects/trim_galore/
2. Krueger F, Andrews SR. Bismark: a flexible aligner and methylation caller for Bisulfite-Seq applications. Bioinformatics. 2011;27:1571–2.
3. Kent WJ, Zweig AS, Barber G, Hinrichs AS, Karolchik D. BigWig and BigBed: enabling browsing of large distributed datasets. Bioinformatics. 2010;26:2204–7.
