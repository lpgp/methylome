#!/usr/bin/env nextflow

// fastqc
process FASTQC {

	publishDir "${params.out_dir}/fastqc", mode: 'copy', overwrite: true

	input:
	tuple val(replicateId), path(reads) 

	output:
	path '*.html'

	script:
	"""
	conda run -n env fastqc -t 2 ${reads}
	"""
}

// read trimming
process TRIM {

	input:
	tuple val(replicateId), path(reads) 

	output:
	tuple val(replicateId), path('*.fq.gz') 

	script:
 	def c_r1 = params.clip_r1 > 0 ? "--clip_r1 ${params.clip_r1}" : ''
    def c_r2 = params.clip_r2 > 0 ? "--clip_r2 ${params.clip_r2}" : ''
    def tpc_r1 = params.three_prime_clip_r1 > 0 ? "--three_prime_clip_r1 ${params.three_prime_clip_r1}" : ''
	def tpc_r2 = params.three_prime_clip_r2 > 0 ? "--three_prime_clip_r2 ${params.three_prime_clip_r2}" : ''
 	def rrbs = params.rrbs ? "--rrbs" : ''
	def keep_trimmed = params.keep_trimmed ? 'html,txt,fq.gz' : 'html,txt'

	"""
	conda run -n env trim_galore \
	-q 30 \
	--paired \
	--fastqc \
	--gzip \
	-j 4 \
	${c_r1} \
	${c_r2} \
	${tpc_r1} \
	${tpc_r2} \
	${rrbs} \
	${reads}

	mkdir -p "${params.out_dir}/trimfastq"
	cp *.{${keep_trimmed}} "${params.out_dir}/trimfastq"
	"""
}

// bismark index
process GENOME_INDEX {

	if(params.keep_index){

		publishDir "${params.out_dir}", mode: 'copy', overwrite: true

	}

	input:
	path genome

	output:
	path 'BismarkIndex', type: 'dir'

	script:
	"""
	mkdir BismarkIndex
    cp ${genome} BismarkIndex/

	conda run -n env bismark_genome_preparation \
    --parallel 6 \
	BismarkIndex
	"""
}

// Bismark map
process MAP {
	tag "${replicateId}"

	input:
	path genome
	tuple val(replicateId), path(reads)

	output:
	tuple val(replicateId), path('*.bam')

	script:
	def local = params.local ? "--local" : ''

	"""
	conda run -n env bismark \
	--genome ${genome} \
	--temp_dir temp \
	--parallel 8 \
	--score_min L,0,-${params.score_min} \
	${local} \
	-1 ${reads[0]} \
	-2 ${reads[1]}

	mkdir -p "${params.out_dir}/bam"
	cp *.txt "${params.out_dir}/bam"
	"""
}

// Bismark deduplicate
process DEDUPLICATE{
	tag "${replicateId}"

	input:
	tuple val(replicateId), path(bam)

	output:
	tuple val(replicateId), path('*.bam')

	script:
	"""
	conda run -n env deduplicate_bismark \
	--bam \
	${bam}

	mkdir -p "${params.out_dir}/deduplicated"
	cp *.txt "${params.out_dir}/deduplicated"
	"""
}

// Bismark count CpG
process COUNT{
	tag "${replicateId}"

	input:
	path genome_index
	tuple val(replicateId), path(bam)

	output:
	tuple val(replicateId), path('*count.txt.gz')

	script:
	def ignore = params.ignore > 0 ? "--ignore ${params.ignore}" : ''
    def ignore_r2 = params.clip_r2 > 0 ? "--ignore_r2 ${params.ignore_r2}" : ''
	def ignore_3prime = params.ignore > 0 ? "--ignore_3prime ${params.ignore_3prime}" : ''
    def ignore_3prime_r2 = params.clip_r2 > 0 ? "--ignore_3prime_r2 ${params.ignore_3prime_r2}" : ''
	def scaffolds = params.scaffolds ? "--scaffolds" : ''
	def buffer_size = params.buffer_size !=false ? "--buffer_size ${params.buffer_size}" : ''
	def CX = params.CX ? "--CX" : ''

	"""
	conda run -n env bismark_methylation_extractor \
	--comprehensive \
	--bedGraph \
	--cytosine_report \
	${CX} \
 	--gzip \
	--parallel 6 \
	--genome_folder ${genome_index} \
	${buffer_size} \
	${scaffolds} \
	${ignore} \
	${ignore_r2} \
	${ignore_3prime} \
	${ignore_3prime_r2} \
	${bam}

	zcat *_report.txt.gz | \
	awk 'BEGIN{print "chr\\tpos\\tstrand\\tN\\tX";OFS = "\\t"}{if(\$4+\$5>0){print \$1,\$2,\$3,\$4+\$5,\$4}}' | \
	gzip > ${replicateId}".count.txt.gz"

	mkdir -p "${params.out_dir}/count"
	cp *{count.txt.gz,report.txt.gz,.txt} "${params.out_dir}/count"
	"""
}

// chromosomes size
process CHR_SIZE {

	input:
	path genome

	output:
	path "chr_size.txt"

	script:
	"""
    zcat ${genome}/*.fa.gz | sed 's/ .*//g' > genome.fa
	conda run -n env seqkit fx2tab -l -n genome.fa > chr_size.txt
	sed -i "s/ .*REF//g" chr_size.txt
	"""
}

// bigwig
process BIG_WIG {
	tag "${replicateId}"

	input:
	path genome
	tuple val(replicateId), path(input)

	output:
	path "*.bed"

	script:
	"""
	zcat ${input} | \
	awk 'NR>1{print \$1,\$2-1,\$2,\$5/\$4}' | \
	sort -u -k1,1 -k2,2n  > ${replicateId}.bed
	awk 'BEGIN{print "track type=bedGraph"}{print}' ${replicateId}.bed > sample
	bedGraphToBigWig sample ${genome} ${replicateId}.bw

	mkdir -p "${params.out_dir}/bw"
	cp *.{bw,bed} "${params.out_dir}/bw"
	"""
}

// Merge samples ratio
process MERGE_RATIO {
	tag "all samples"

    publishDir "${params.out_dir}/merged_samples_ratio", mode: 'copy', overwrite: true

	input:
	path input

	output:
	path "*.txt"

	script:
	"""
	Rscript --vanilla $baseDir/bin/merge_samples_ratio.R
	"""
}
