#!/usr/bin/env nextflow

// Enable DSL 2 syntax
nextflow.enable.dsl = 2

// Import modules 
include { 
	GENOME_INDEX;
	FASTQC;
	TRIM;
	MAP;
	DEDUPLICATE;
	COUNT;
  	CHR_SIZE;
  	BIG_WIG;
	MERGE_RATIO
} from "./modules/modules.nf"

// output directory
file("${params.out_dir}").mkdirs()

// main pipeline logic
workflow {

	// fastq input
	reads=Channel.fromPath(params.input)
		.splitCsv(header:true, sep:',')
    	.map {
        	row -> tuple(
            	row.ID,
            	[
					file(row.R1,checkIfExists: true),
					file(row.R2,checkIfExists: true),
				]
        	)
    	}

	// genome index channel
	if(params.bismarkIndex ==false & params.genome!=false){

		GENOME_INDEX (
			params.genome
		)

		genome_index = GENOME_INDEX.out

	}else{

		genome_index = params.bismarkIndex

	}

	//raw fastQC
	if(params.skip_fastqc==false){

		FASTQC(
			reads
		)
	
	}

	// trimming
	if(params.skip_trimming==false){

		TRIM (
			reads
		)

		tomap_ch=TRIM.out
		
	}else{

		tomap_ch=reads
	}

	// genome Alignment
	MAP(
		genome_index,
		tomap_ch
	)

	// deduplicate
	if(params.rrbs==false){

		DEDUPLICATE(
			MAP.out	
		)

		bam_ch = DEDUPLICATE.out

	}else{

		bam_ch = MAP.out

	}

	// CpG report
	COUNT(
		genome_index,
		bam_ch
	)

	if (params.bigwig){
		// chromosomes size
		CHR_SIZE(
			genome_index
		)

		// make bigwig
		BIG_WIG(
			CHR_SIZE.out,
			COUNT.out
		)
	}

	// merge
	if (params.meth_ratio_matrix){

		MERGE_RATIO(
			BIG_WIG.out.collect()
		)
	}
}